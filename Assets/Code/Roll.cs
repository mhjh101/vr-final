﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roll : MonoBehaviour
{
    public Vector3 playerStartPosition;
    public float movementSpeed = 4;
    //public GameObject winScreen;
    //public GameObject OpeningScreen;
    //public GameObject pickupEffect;

    private int spawnedPickupCount;
    Rigidbody _rb;
    private int count = 0;

    private bool allowedMovement = true;
    private bool gameHasStarted = false;

    public LayerMask ground;
    public SphereCollider _col;

    // Start is called before the first frame update
    void Start()
    {
        //OpeningScreen.SetActive(true);

        this.movementSpeed = 10;
        _rb = GetComponent<Rigidbody>();
        _col = GetComponent<SphereCollider>();
    }
    public void StartGame()
    {
       //OpeningScreen.SetActive(false);
}
//Update is called once per frame
void Update()
    {
        Move();
    }
    private void Move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

        _rb.AddForce(movement * movementSpeed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            Destroy(other.gameObject);
        }
    }
}
