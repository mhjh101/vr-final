﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class s : MonoBehaviour
{
    public GameObject pickupPrefab;
    public Transform pickupParent;
    public List<Transform> spawnLocations;
    public List<GameObject> spawnedPickups;

    public UnityEvent pickupsSpawned;

    [Space(10)]
    public bool _debug;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SpawnPickups()
    {
        
    }
    public void DestroyPickups()
    {
        foreach (GameObject PickUp in spawnedPickups)
        {
            Destroy(PickUp);
        }
        spawnedPickups.Clear();
    }
}