﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorCollider : MonoBehaviour
{
    public float DoorClosed;
    private bool DoesDoorCloses = true;
    private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TriggerDoor()
    {
        if (DoesDoorCloses)
        {
            StartCoroutine(ElevatorDoorEvent());
        }
    }

        IEnumerator ElevatorDoorEvent()
    {
        DoesDoorCloses = false;

        _anim.SetBool("ElevatorClosing", true);

        float clipPlaytime = _anim.GetCurrentAnimatorStateInfo(0).length;
        float elapsedTime = 0;
        while(elapsedTime <= clipPlaytime)
        {
            yield return null;
            elapsedTime += Time.deltaTime;
        }
        yield return new WaitForSeconds(DoorClosed);
        _anim.SetTrigger("DoorClosed");

        yield break;
    }
    
}
