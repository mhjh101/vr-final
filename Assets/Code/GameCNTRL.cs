﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameCNTRL : MonoBehaviour
{
    public GameObject winScreen;
    public GameObject OpeningScreen;

    public UnityEvent reset;

    public Roll playerControll;

    // Start is called before the first frame update
    void Start()
    {
        OpeningScreen.SetActive(true);
        winScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GameReset()
    {
        reset.Invoke();
    }
    public void StartGame()
    {
        OpeningScreen.SetActive(false);

        //playerControll.ToggleCharacterMovement(true);
        
    }
}
